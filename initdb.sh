#!/bin/bash
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER"  <<-EOSQL

    CREATE USER $USER_NAME WITH
	LOGIN
	SUPERUSER
	CREATEDB
	CREATEROLE
	INHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD '$USER_PASSWORD';

	CREATE DATABASE $DB
    WITH
    OWNER = $USER_NAME
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

  CREATE DATABASE $DB_TEST
    WITH
    OWNER = $USER_NAME
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

EOSQL