package com.starexchangealliance.impl;

import com.starexchangealliance.UserService;
import com.starexchangealliance.interfaces.UserDao;
import com.starexchangealliance.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public User getById(int id) {
        return userDao.getId(id);
    }

    @Override
    public User createUser(User user) {
        return userDao.insert(user);
    }

    @Override
    public boolean update(User user) {
        return userDao.update(user) > 0;
    }

    @Override
    public boolean deleteUserById(int id) {
        return userDao.deleteById(id) > 0;
    }
}
