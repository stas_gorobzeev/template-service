package com.starexchangealliance.impl;

import com.starexchangealliance.PetService;
import com.starexchangealliance.interfaces.PetDao;
import com.starexchangealliance.model.Pet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PetServiceImpl implements PetService {

    @Autowired
    private PetDao petDao;

    @Override
    public Pet getById(int id) {
        return petDao.getId(id);
    }

    @Override
    public Pet createPet(Pet pet) {
        return petDao.insert(pet);
    }

    @Override
    public boolean update(Pet pet) {
        return petDao.update(pet) > 0;
    }

    @Override
    public boolean deletePetById(int id) {
        return petDao.deleteById(id) > 0;
    }
}
