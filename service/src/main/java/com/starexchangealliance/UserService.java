package com.starexchangealliance;

import com.starexchangealliance.model.User;

public interface UserService {

    User getById(int id);

    User createUser(User user);

    boolean update(User user);

    boolean deleteUserById(int id);
}
