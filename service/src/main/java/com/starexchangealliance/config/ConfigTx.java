package com.starexchangealliance.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Configuration
public class ConfigTx {

    @Autowired
    DataSource dataSource;

    @Bean
    public PlatformTransactionManager masterPlatformTransactionManager() {
        return new DataSourceTransactionManager(dataSource);
    }
}
