package com.starexchangealliance;

import com.starexchangealliance.model.Pet;

public interface PetService {

    Pet getById(int id);

    Pet createPet(Pet pet);

    boolean update(Pet pet);

    boolean deletePetById(int id);
}
