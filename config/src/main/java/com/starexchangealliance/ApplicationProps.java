package com.starexchangealliance;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationProps {

    @Value("${spring.datasource.driver-class-name}")
    private String datasourceDriverClassName;

    @Value("${spring.datasource.server-name}")
    private String serverName;

    @Value("${spring.datasource.server-port}")
    private String serverPort;

    @Value("${spring.datasource.db-name}")
    private String dbName;

    @Value("${spring.datasource.username}")
    private String datasourceUsername;

    @Value("${spring.datasource.password}")
    private String datasourcePassword;

    public String getDatasourceDriverClassName() {
        return datasourceDriverClassName;
    }

    public String getServerName() {
        return serverName;
    }

    public String getServerPort() {
        return serverPort;
    }

    public String getDbName() {
        return dbName;
    }

    public String getDatasourceUsername() {
        return datasourceUsername;
    }

    public String getDatasourcePassword() {
        return datasourcePassword;
    }
}
