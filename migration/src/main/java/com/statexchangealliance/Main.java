package com.statexchangealliance;

import com.starexchangealliance.ApplicationProps;
import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.beans.factory.annotation.Autowired;

public class Main {

    @Autowired
    ApplicationProps props;

    public void start() {
        PGSimpleDataSource ds = new PGSimpleDataSource();
        ds.setServerName(props.getServerName());
        ds.setDatabaseName(props.getDbName());
        ds.setUser(props.getDatasourceUsername());
        ds.setPassword(props.getDatasourcePassword());
        ds.setPortNumber(Integer.parseInt(props.getServerPort()));
        new Migration().apply(ds, props.getServerName() + ":" + props.getServerPort() + "/" + props.getDbName());
    }
}
