CREATE TABLE Pet
(
    "id"        SERIAL PRIMARY KEY,
    "name"      VARCHAR(256)          DEFAULT NULL,
    "message"   VARCHAR(256)          DEFAULT NULL,
    "timestamp" TIMESTAMP(6) NOT NULL DEFAULT now()
);

CREATE TABLE Users
(
    "id"        SERIAL PRIMARY KEY,
    "name"      VARCHAR(256) NOT NULL,
    "email"     VARCHAR(256) NOT NULL,
    "password"  VARCHAR(256) NOT NULL,
    "create_at" TIMESTAMP(6) NOT NULL DEFAULT now(),
    "update_at" TIMESTAMP(6) NOT NULL DEFAULT now()
);