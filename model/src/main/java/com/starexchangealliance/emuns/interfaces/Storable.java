package com.starexchangealliance.emuns.interfaces;

public interface Storable {

    Object valueToStore();

}