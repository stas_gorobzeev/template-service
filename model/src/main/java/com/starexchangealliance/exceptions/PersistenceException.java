package com.starexchangealliance.exceptions;

public class PersistenceException extends RuntimeException {

    public final String id;

    public PersistenceException(String id, String message) {
        super(message);
        this.id = id;
    }

}
