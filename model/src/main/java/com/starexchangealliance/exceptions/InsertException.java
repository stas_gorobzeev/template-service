package com.starexchangealliance.exceptions;

public class InsertException extends PersistenceException {

    private static final String ID = "INSERT_ERROR";

    public InsertException(String message) {
        super(ID, message);
    }

}
