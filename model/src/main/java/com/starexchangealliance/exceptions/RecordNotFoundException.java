package com.starexchangealliance.exceptions;

public class RecordNotFoundException extends PersistenceException {

    private static final String ID = "RECORD_NOT_EXISTS";

    public RecordNotFoundException(String message) {
        super(ID, message);
    }

}
