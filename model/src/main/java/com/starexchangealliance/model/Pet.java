package com.starexchangealliance.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

@Builder
@Getter
@Setter
public class Pet extends ID {

    private String name;
    private String message;
    private DateTime timestamp;

    public Pet(int id, String name, String message, DateTime timestamp) {
        super(id);
        this.name = name;
        this.message = message;
        this.timestamp = timestamp;
    }
}


