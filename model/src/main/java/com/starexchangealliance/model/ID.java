package com.starexchangealliance.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public abstract class ID {

    protected int id;

    public ID(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    final public boolean equals(Object o) {
        return this == o || !(o == null || getClass() != o.getClass()) && id == ((ID) o).id;
    }

    @Override
    final public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
