package com.starexchangealliance.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

@Getter
@Setter
@Builder
public class User extends ID {
    private String name;
    private String email;
    private String password;
    private DateTime createAt;
    private DateTime updateAt;

    public User(int id, String name, String email, String password, DateTime createAt, DateTime updateAt) {
        super(id);
        this.name = name;
        this.email = email;
        this.password = password;
        this.createAt = createAt;
        this.updateAt = updateAt;
    }
}
