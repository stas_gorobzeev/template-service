package com.starexchangealliance.jdbc;

import com.google.common.base.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class SQLProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(SQLProvider.class);

    private NamedParameterJdbcOperations jdbcTemplate;
    private String daoName = getClass().getSimpleName();

    protected NamedParameterJdbcOperations getJdbcTemplate() {
        return jdbcTemplate;
    }

    /**
     * Run an UPDATE statement
     * @param sql Update statement
     * @param params Input to UPDATE
     * @return
     */
    protected int apply(String sql, Map<String, Object> params) {
        log("APPLY", sql, params);

        return getJdbcTemplate().update(sql, params);
    }

    /**
     * Run a query for one or more rows with no input
     * @param sql SQL query as a string
     * @param mapper class to map resultset to POJO
     * @param <E> Type of result (extracted from mapper)
     * @return Result rows
     */
    protected  <E> List<E> list(String sql, RowMapper<E> mapper) {
        return list(sql, Collections.<String, Object>emptyMap(), mapper);
    }

    /**
     * Run a query for one or more rows with a map as input
     * @param sql SQL query as a string
     * @param params Map of key/value pairs as query input
     * @param mapper class to map resultset to POJO
     * @param <E> Type of result (extracted from mapper)
     * @return Result rows
     */
    protected  <E> List<E> list(String sql, Map<String, Object> params, RowMapper<E> mapper) {
        log("LIST", sql, params);

        return getJdbcTemplate().query(sql, params, mapper);
    }

    protected int count(String sql, Map<String, Object> params) {
        log("COUNT", sql, params);
        try {
            return getJdbcTemplate().queryForObject(sql, params, Integer.class);
        } catch (EmptyResultDataAccessException e) {
            log("COUNT " + e.getMessage());
            return 0;
        }
    }

    /**
     * Run SQL query with a set as query input
     * @param sql SQL query as a string
     * @param ids Set of id's to pass to sql
     * @param mapper Type of result (extracted from mapper)
     * @param <E> Type of result (extracted from mapper)
     * @return List of result rows
     */
    protected <E> List<E> listIn(String sql, Set<?> ids, RowMapper<E> mapper) {
        if (ids == null || ids.isEmpty()) {
            log("LIST IN (?:ids) IS NULL OR EMPTY");
            return Collections.emptyList();
        }

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ids", ids);
        return query(sql, parameters, mapper);
    }

    /**
     * Run SQL query with a set as query input
     * @param sql SQL query as a string
     * @param parameters Arguments for query
     * @param mapper Type of result (extracted from mapper)
     * @param <E> Type of result (extracted from mapper)
     * @return List of result rows
     */
    protected <E> List<E> query(String sql, MapSqlParameterSource parameters, RowMapper<E> mapper) {
        log("QUERY ", sql, parameters.getValues());

        return getJdbcTemplate().query(sql, parameters, mapper);
    }

    /**
     * Query expecting one result row/object, no param is passed to query
     * @param sql Query to use
     * @param mapper class to map resultset to POJO
     * @param <E> Type of result (extracted from mapper)
     * @return Single result object
     */
    protected  <E> E one(String sql, RowMapper<E> mapper) {
        return one(sql, Collections.<String, Object>emptyMap(), mapper);
    }

    /**
     * Query expecting one result row/object, params to SQL query passed in
     * @param sql Query to use
     * @param params parameters to pass to query
     * @param mapper class to map resultset to POJO
     * @param <E> Type of result (extracted from mapper)
     * @return Single result object
     */
    protected  <E> E one(String sql, Map<String, Object> params, RowMapper<E> mapper) {
        log("GET OBJECT", sql, params);

        return getJdbcTemplate().queryForObject(sql, params, mapper);
    }

    /**
     * find an object (row) wrapped in an Optional
     * @param sql SQL query as a string
     * @param params Map of key/value pairs as query input
     * @param mapper class to map resultset to POJO
     * @param <E> Type of result (extracted from mapper)
     * @return Optional wrapped result
     */
    protected <E> Optional<E> oneOptional(final String sql, final Map<String, Object> params, final RowMapper<E> mapper) {
        log("GET OBJECT OPTIONAL", sql, params);

        return optional(() -> getJdbcTemplate().queryForObject(sql, params, mapper));
    }

    interface Producer<T> {
        T apply();
    }

    protected <E> Optional<E> optional(Producer<E> function) {
        try {
            return Optional.of(function.apply());
        } catch (EmptyResultDataAccessException e) {
            log("GET OBJECT OPTIONAL: EmptyResultDataAccessException " + e.getMessage());
            return Optional.absent();
        }
    }

    private void log(String command, String sql, Object arguments) {
        LOGGER.info(String.format("Target [%-15s] Command [%-20s] \n SQL: %s Args: %s",
                daoName, command, sql, String.valueOf(arguments)));
    }

    private void log(String info) {
        LOGGER.info(info);
    }

    @Required
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
}
