package com.starexchangealliance.jdbc;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class ExtendedRowMapper<E> implements RowMapper<E> {

    private final ResultSetReader reader = new ResultSetReaderImpl();

    @Override
    public E mapRow(ResultSet rs, int idx) throws SQLException {
        reader.setRs(rs);
        return mapRow(reader);
    }

    public abstract E mapRow(ResultSetReader rs) throws SQLException;
}
