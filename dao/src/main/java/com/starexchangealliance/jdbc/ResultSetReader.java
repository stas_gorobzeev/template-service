package com.starexchangealliance.jdbc;

import com.google.common.base.Optional;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public interface ResultSetReader {

    int getInt(String name) throws SQLException;
    BigDecimal getDecimal(String name) throws SQLException;
    String getString(String name) throws SQLException;
    boolean getBoolean(String name) throws SQLException;
    DateTime getDateTime(String name) throws SQLException;
    char getChar(String name) throws SQLException;
    long getLong(String name) throws SQLException;
    UUID getUUID(String name) throws SQLException;
    LocalDate getLocalDate(String name)  throws SQLException;

    Optional<Integer> getIntOpt(String name) throws SQLException;
    Optional<String> getStringOpt(String name) throws SQLException;
    Optional<DateTime> getDateTimeOpt(String name) throws SQLException;
    Optional<LocalDate> getLocalDateOpt(String name) throws SQLException;
    Optional<JsonObject> getJSONOpt(String name) throws SQLException;

    void setRs(ResultSet rs);
}
