package com.starexchangealliance.jdbc;

import com.google.common.base.Optional;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.UUID;

public class ResultSetReaderImpl implements ResultSetReader {

    public static abstract class Reader<T, From> {
        public Optional<T> readOpt(ResultSet rs, String name) throws SQLException {
            From result = read(rs, name);
            return result == null || rs.wasNull() ? Optional.<T>absent() : Optional.of(map(result));
        }

        abstract From read(ResultSet rs, String name) throws SQLException;

        abstract T map(From from) throws SQLException;
    }

    public static abstract class SelfReader<T> extends Reader<T, T> {
        final T map(T v) throws SQLException {
            return v;
        }
    }

    private static final Reader<Integer, Integer> INTEGER_READER = new SelfReader<Integer>() {
        @Override
        public Integer read(ResultSet rs, String name) throws SQLException {
            return rs.getInt(name);
        }
    };

    private static final Reader<BigDecimal, BigDecimal> DECIMAL_READER = new SelfReader<BigDecimal>() {
        @Override
        public BigDecimal read(ResultSet rs, String name) throws SQLException {
            return rs.getBigDecimal(name);
        }
    };

    private static final Reader<String, String> STRING_READER = new SelfReader<String>() {
        @Override
        public String read(ResultSet rs, String name) throws SQLException {
            return rs.getString(name);
        }
    };

    private static final Reader<DateTime, Timestamp> DATETIME_READER = new Reader<DateTime, Timestamp>() {
        @Override
        public Timestamp read(ResultSet rs, String name) throws SQLException {
            return rs.getTimestamp(name);
        }

        @Override
        DateTime map(Timestamp timestamp) throws SQLException {
            return new DateTime(timestamp);
        }
    };

    private static final Reader<UUID, String> UUID_READER = new Reader<UUID, String>() {
        @Override
        public String read(ResultSet rs, String name) throws SQLException {
            return rs.getString(name);
        }

        @Override
        UUID map(String v) throws SQLException {
            return UUID.fromString(v);
        }
    };
    private static final Reader<Boolean, Boolean> BOOLEAN_READER = new SelfReader<Boolean>() {
        @Override
        public Boolean read(ResultSet rs, String name) throws SQLException {
            return rs.getBoolean(name);
        }
    };

    private static final Reader<Long, Long> LONG_READER = new SelfReader<Long>() {
        @Override
        public Long read(ResultSet rs, String name) throws SQLException {
            return rs.getLong(name);
        }
    };

    private static final Reader<LocalDate, Date> LOCAL_DATE_READER = new Reader<LocalDate, Date>() {
        @Override
        public Date read(ResultSet rs, String name) throws SQLException {
            return rs.getDate(name);
        }

        @Override
        LocalDate map(Date v) throws SQLException {
            return LocalDate.fromDateFields(v);
        }
    };

    private static final Reader<Character, String> CHARACTER_READER = new Reader<Character, String>() {
        @Override
        public String read(ResultSet rs, String name) throws SQLException {
            String value = STRING_READER.read(rs, name);
            if (value == null) {
                return " "; // TODO FIXXXXXXXXXXXXXXXXXXX ME (null will throw an exception)
            }
            if (value.isEmpty() || value.length() != 1) {
                throw new IllegalArgumentException("Incorrect character value");
            }
            return value;
        }

        @Override
        Character map(String v) throws SQLException {
            if (v == null)
                return null;
            return v.charAt(0);
        }
    };

    private static final Reader<JsonObject, String> JSON_OBJECT_READER = new Reader<JsonObject, String>() {
        @Override
        public String read(ResultSet rs, String name) throws SQLException {
            String object = rs.getString(name);
            return object;

        }
        @Override
        JsonObject map(String v) throws SQLException {
            JsonObject obj = new JsonParser().parse(v).getAsJsonObject();
            return obj;
        }
    };

    private ResultSet rs;

    @Override
    public void setRs(ResultSet rs) {
        this.rs = rs;
    }

    public ResultSet getRs() {
        return rs;
    }

    @Override
    public int getInt(String name) throws SQLException {
        return apply(INTEGER_READER, name);
    }

    @Override
    public BigDecimal getDecimal(String name) throws SQLException {
        return apply(DECIMAL_READER, name);
    }

    @Override
    public String getString(String name) throws SQLException {
        return apply(STRING_READER, name);
    }

    @Override
    public char getChar(String name) throws SQLException {
        return apply(CHARACTER_READER, name);
    }

    @Override
    public boolean getBoolean(String name) throws SQLException {
        return apply(BOOLEAN_READER, name);
    }

    @Override
    public DateTime getDateTime(String name) throws SQLException {
        return apply(DATETIME_READER, name);
    }

    @Override
    public long getLong(String name) throws SQLException {
        return apply(LONG_READER, name);
    }

    @Override
    public UUID getUUID(String name) throws SQLException {
        return apply(UUID_READER, name);
    }

    @Override
    public LocalDate getLocalDate(String name) throws SQLException {
        return apply(LOCAL_DATE_READER, name);
    }

    @Override
    public Optional<Integer> getIntOpt(String name) throws SQLException {
        return applyOpt(INTEGER_READER, name);
    }

    @Override
    public Optional<String> getStringOpt(String name) throws SQLException {
        return applyOpt(STRING_READER, name);
    }

    @Override
    public Optional<DateTime> getDateTimeOpt(String name) throws SQLException {
        return applyOpt(DATETIME_READER, name);
    }

    @Override
    public Optional<LocalDate> getLocalDateOpt(String name) throws SQLException {
        return applyOpt(LOCAL_DATE_READER, name);
    }

    @Override
    public Optional<JsonObject> getJSONOpt(String name) throws SQLException {
        Optional<JsonObject> jsonObjectOptional = applyOpt(JSON_OBJECT_READER, name);
        return jsonObjectOptional;
    }

    private <T, From> T apply(Reader<T, From> reader, String name) throws SQLException {
        return reader.map(reader.read(getRs(), name));
    }

    private <T, From> Optional<T> applyOpt(Reader<T, From> reader, String name) throws SQLException {
        Optional<T> tOptional = reader.readOpt(getRs(), name);
        return tOptional;
    }
}
