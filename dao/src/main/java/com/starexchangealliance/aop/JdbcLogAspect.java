package com.starexchangealliance.aop;

import co.elastic.apm.api.ElasticApm;
import co.elastic.apm.api.Span;
import com.starexchangealliance.aop.logging.MethodsLog;
import com.starexchangealliance.aop.loggingTxContext.QuerriesCountThreadLocal;
import com.starexchangealliance.aop.logs.LoggingUtils;
import com.starexchangealliance.processidmanager.ProcessIDManager;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Map;

@Aspect
@Component
public class JdbcLogAspect {
    private static final Logger log = LogManager.getLogger(JdbcLogAspect.class);

    private static final long SLOW_QUERY_THREESHOLD_MS = 1000;

    @Pointcut("execution(* org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate..*(..))" +
            "(execution(* org.springframework.jdbc.core.JdbcOperations..*(..))))")
    public void jdbc() {
    }

    @Around("jdbc()")
    public Object doBasicProfiling(ProceedingJoinPoint pjp) throws Throwable {
        String method = LoggingUtils.getMethodName(pjp);
        String args = formatQuery(pjp.getArgs());
        long start = System.currentTimeMillis();
        String user = LoggingUtils.getAuthenticatedUser();
        Object result = null;
        String exStr = StringUtils.EMPTY;
        String currentProcessId = ProcessIDManager.getProcessIdFromCurrentThread().orElse(StringUtils.EMPTY);
        Span span = ElasticApm.currentSpan().startSpan("db", "postgresql", "query").setName(args);
        MethodsLog mLog;
        try {
            result = pjp.proceed();
            span.addLabel("process_id", currentProcessId);
            span.addLabel("fullSql", args);
            return result;
        } catch (Throwable ex) {
            span.captureException(ex);
            exStr = LoggingUtils.formatException(ex);
            throw ex;
        } finally {
            QuerriesCountThreadLocal.inc();
            long exTime = LoggingUtils.getExecutionTime(start);
            mLog = new MethodsLog(method, args, result, user, exTime, exStr);
            log.info(mLog);
            logSlowQuery(span, exTime);
            span.end();
        }
    }

    private String formatQuery(Object[] args) {
        try {
            String sql = (String) args[0];
            Map<String, ?> querryArgs = (Map<String, ?>) args[1];
            return LoggingUtils.completeSql(sql, querryArgs);
        } catch (Exception e) {
            return Arrays.toString(args);
        }
    }

    private void logSlowQuery(Span span, long execTime) {
        if (execTime > SLOW_QUERY_THREESHOLD_MS) {
            span.addLabel("slow_query", execTime);
        }
    }
}