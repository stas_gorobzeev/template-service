package com.starexchangealliance.aop.logs;


import com.starexchangealliance.aop.logging.LogsWrapper;

public interface UserLogsHandler {

    void onUserLogEvent(LogsWrapper logsWrapper);
}
