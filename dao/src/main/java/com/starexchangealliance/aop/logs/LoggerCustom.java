package com.starexchangealliance.aop.logs;

import com.starexchangealliance.aop.logging.MethodsLog;
import com.starexchangealliance.processidmanager.ProcessIDManager;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;
import java.util.function.Supplier;


@Log4j2(topic = "Service_layer_log")
public class LoggerCustom {

    private static final Logger logger = LogManager.getLogger(LoggerCustom.class);

    public static Object logAndExecute(Supplier supp, String methodName, Class clazz, String args) {
        long start = System.currentTimeMillis();
        String user = LoggingUtils.getAuthenticatedUser();
        ProcessIDManager
                .getProcessIdFromCurrentThread()
                .orElseGet(() -> {
                    ProcessIDManager.registerNewThreadForParentProcessId(clazz, Optional.empty());
                    return StringUtils.EMPTY;
                });
        try {
            Object result = supp.get();
            logger.debug(new MethodsLog(methodName, args, result, user, LoggingUtils.getExecutionTime(start), StringUtils.EMPTY));
            return result;
        } catch (Throwable ex) {
            logger.debug(new MethodsLog(methodName, args, StringUtils.EMPTY, user, LoggingUtils.getExecutionTime(start), ex.getCause() + " " + ex.getMessage()));
            throw ex;
        } finally {
            ProcessIDManager.unregisterProcessId(clazz);
        }
    }
}
