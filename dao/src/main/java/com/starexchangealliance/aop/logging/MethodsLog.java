package com.starexchangealliance.aop.logging;


import lombok.Data;

@Data
public class MethodsLog {

    private String methodName;
    private String arguments;
    private Object result;
    private String userEmail;
    private long processingTime;
    private String error;

    public MethodsLog(String methodName, String arguments, Object result, String userEmail, long processingTime, String error) {
        this.methodName = methodName;
        this.arguments = arguments;
        this.result = result;
        this.userEmail = userEmail;
        this.processingTime = processingTime;
        this.error = error;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MethodsLog{");
        sb.append("methodName='").append(methodName).append('\'');
        sb.append(", arguments='").append(arguments).append('\'');
        sb.append(", result=").append(result);
        sb.append(", userEmail='").append(userEmail).append('\'');
        sb.append(", processingTime=").append(processingTime);
        sb.append(", error='").append(error).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
