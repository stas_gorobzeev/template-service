package com.starexchangealliance.aop.logging;

public enum LogsTypeEnum {

   WS_SUBSCRIBE, HTTP_REQUEST_LOG, SQL_QUERY, HTTP_ERROR_HANDLER_RESPONSE;
}
