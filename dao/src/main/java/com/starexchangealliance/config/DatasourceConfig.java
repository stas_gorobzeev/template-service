package com.starexchangealliance.config;

import com.starexchangealliance.ApplicationProps;
import com.statexchangealliance.Migration;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DatasourceConfig {

    @Autowired
    private ApplicationProps props;

    @Bean
    public DataSource hikariDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(props.getDatasourceDriverClassName());
        hikariConfig.setJdbcUrl("jdbc:postgresql://" + props.getServerName() + ":" + props.getServerPort() + "/" + props.getDbName());
        hikariConfig.setUsername(props.getDatasourceUsername());
        hikariConfig.setPassword(props.getDatasourcePassword());
        hikariConfig.setMaximumPoolSize(50);
        HikariDataSource dataSource = new HikariDataSource(hikariConfig);
        new Migration().apply(dataSource, "");
        return dataSource;
    }

}