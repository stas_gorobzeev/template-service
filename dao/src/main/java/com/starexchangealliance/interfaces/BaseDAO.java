package com.starexchangealliance.interfaces;

import com.google.common.base.Optional;
import com.starexchangealliance.exceptions.RecordNotFoundException;

import java.util.List;

public interface BaseDAO<E, K> {

    List<Integer> ids();

    E insert(E e);

    List<E> insert(List<E> e);

    int update(E e);

    int update(List<E> e);

    int delete(E e);

    int delete(List<E> e);

    int deleteById(K id);

    Optional<E> getIdOpt(K id);

    E getId(K id) throws RecordNotFoundException;

    E getId(K id, String msg) throws RecordNotFoundException;

    List<E> fetch();

    boolean exists(K id);
}
