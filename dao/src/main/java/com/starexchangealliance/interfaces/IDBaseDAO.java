package com.starexchangealliance.interfaces;


import com.starexchangealliance.model.ID;

public interface IDBaseDAO<E extends ID> extends BaseDAO<E, Integer> {
    String ID = "id";
}
