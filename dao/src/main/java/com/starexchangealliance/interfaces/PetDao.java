package com.starexchangealliance.interfaces;

import com.starexchangealliance.model.Pet;

public interface PetDao extends IDBaseDAO<Pet> {
    String NAME = "name";
    String MESSAGE = "message";
    String TIMESTAMP = "timestamp";
}
