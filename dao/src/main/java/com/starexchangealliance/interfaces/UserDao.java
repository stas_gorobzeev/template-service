package com.starexchangealliance.interfaces;

import com.starexchangealliance.model.User;

public interface UserDao extends IDBaseDAO<User> {
    String NAME = "name";
    String EMAIL = "email";
    String PASSWORD = "password";
    String UPDATE_AT = "updated_at";
    String CREATE_AT = "create_at";
}
