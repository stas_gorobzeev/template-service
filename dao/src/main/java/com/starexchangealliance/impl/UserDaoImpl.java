package com.starexchangealliance.impl;

import com.starexchangealliance.interfaces.UserDao;
import com.starexchangealliance.jdbc.AbstractIDBaseDAO;
import com.starexchangealliance.jdbc.ResultSetReader;
import com.starexchangealliance.model.User;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;

@Repository
public class UserDaoImpl extends AbstractIDBaseDAO<User> implements UserDao {

    public UserDaoImpl() {
        super("User", User.class);
    }

    @Override
    protected User init(ResultSetReader rs) throws SQLException {
        return new User(
                rs.getInt(ID),
                rs.getString(NAME),
                rs.getString(EMAIL),
                rs.getString(PASSWORD),
                rs.getDateTime(CREATE_AT),
                rs.getDateTime(UPDATE_AT)
        );
    }
}
