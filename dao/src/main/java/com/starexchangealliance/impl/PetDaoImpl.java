package com.starexchangealliance.impl;

import com.starexchangealliance.interfaces.PetDao;
import com.starexchangealliance.jdbc.AbstractIDBaseDAO;
import com.starexchangealliance.jdbc.ResultSetReader;
import com.starexchangealliance.model.Pet;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;

@Repository
public class PetDaoImpl extends AbstractIDBaseDAO<Pet> implements PetDao {

    public PetDaoImpl() {
        super("Pet", Pet.class);
    }

    @Override
    protected Pet init(ResultSetReader rs) throws SQLException {
        return new Pet(
                rs.getInt(ID),
                rs.getString(NAME),
                rs.getString(MESSAGE),
                rs.getDateTime(TIMESTAMP)
        );
    }
}
