package dao;

import com.starexchangealliance.impl.PetDaoImpl;
import com.starexchangealliance.interfaces.PetDao;
import com.starexchangealliance.model.Pet;
import config.DataComparisonTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.SQLException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = PetDaoTest.InnerConfig.class)
public class PetDaoTest extends DataComparisonTest {
    private final String PET = "Pet";

    @Autowired
    private PetDao petDao;


    @Override
    protected void before() {
        try {
            truncateTables(PET);
            String sql = "INSERT INTO Pet"
                    + " (name, message)"
                    + " VALUES "
                    + " ('name', 'message)";
            prepareTestData(sql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    @Test
    public void createTest() {
        around()
                .withSQL("SELECT * FROM Pet")
                .run(() -> petDao.insert(getPet()));
    }

    @Test
    public void getByAllTest() {
        around()
                .withObject(getPet())
                .withSQL("SELECT * FROM Pet")
                .run(() -> petDao.fetch());

    }

    @Test
    public void deleteTest() {
        around()
                .withSQL("SELECT * FROM Pet")
                .run(() -> petDao.deleteById(1));
    }

    @Test
    public void updateTest() {
        Pet pet = getPet();
        pet.setName("updateName");
        around()
                .withObject(pet)
                .withSQL("SELECT * FROM Pet")
                .run((() -> petDao.update(pet)));
    }

    private Pet getPet() {
        return Pet.builder()
                .name("name")
                .message("message")
                .build();
    }


    @Configuration
    static class InnerConfig extends AppContextConfig {

        @Autowired
        private NamedParameterJdbcTemplate jdbcTemplate;

        @Override
        protected String getSchema() {
            return "PetDaoTest";
        }

        @Bean
        public PetDao getPetDao() {
            return new PetDaoImpl();
        }
    }
}
