package dao;

import com.starexchangealliance.impl.UserDaoImpl;
import com.starexchangealliance.interfaces.UserDao;
import com.starexchangealliance.model.User;
import config.DataComparisonTest;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.SQLException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = UserDaoTest.InnerConfig.class)
public class UserDaoTest extends DataComparisonTest {
    private final String USER_TABLE = "User";

    @Autowired
    private UserDao userDao;

    @Override
    protected void before() {
        try {
            truncateTables(USER_TABLE);
            String sql = "INSERT INTO User"
                    + " (name, email, password, updated_at, create_at)"
                    + " VALUES "
                    + " ('name', 'email@mail.com', 'password', now(), now())";
            prepareTestData(sql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void createUserTest() {
        around()
                .withSQL("SELECT * FROM User")
                .run(() -> userDao.insert(getUser()));
    }

    @Test
    public void getByAllUsersTest() {
        around()
                .withObject(getUser())
                .withSQL("SELECT * FROM User")
                .run(() -> userDao.fetch());
    }

    @Test
    public void deleteTest() {
        around()
                .withSQL("SELECT * FROM User")
                .run(() -> userDao.deleteById(1));
    }

    @Test
    public void updateTest() {
        User user = getUser();
        user.setName("updateName");
        around()
                .withObject(user)
                .withSQL("SELECT * FROM User")
                .run((() -> userDao.update(user)));
    }

    private User getUser() {
        return User.builder()
                .name("name")
                .email("email@mail.com")
                .password("password")
                .createAt(DateTime.now())
                .updateAt(DateTime.now())
                .build();
    }


    @Configuration
    static class InnerConfig extends AppContextConfig {

        @Autowired
        private NamedParameterJdbcTemplate jdbcTemplate;

        @Override
        protected String getSchema() {
            return "UserDaoTest";
        }

        @Bean
        public UserDao getUserDao() {
            return new UserDaoImpl();
        }
    }
}
