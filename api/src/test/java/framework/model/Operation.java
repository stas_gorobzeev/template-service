package framework.model;

public interface Operation {

    Object applyOperation() throws Exception;

}
