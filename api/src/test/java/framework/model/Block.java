package framework.model;

public interface Block {

    void setComment(String comment);

    String getComment();
}
