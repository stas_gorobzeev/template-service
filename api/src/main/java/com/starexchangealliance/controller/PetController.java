package com.starexchangealliance.controller;

import com.starexchangealliance.PetService;
import com.starexchangealliance.model.Pet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/pet")
@RestController
public class PetController {

    @Autowired
    private PetService petService;

    @GetMapping("/{id}")
    public ResponseEntity getById(@PathVariable("id") int id) {
        return new ResponseEntity<>(petService.getById(id), HttpStatus.OK);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Pet> create(@RequestBody Pet pet) {
        return new ResponseEntity<>(petService.createPet(pet), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody Pet pet) {
        return new ResponseEntity<>(petService.update(pet), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") int id) {
        return new ResponseEntity<>(petService.deletePetById(id), HttpStatus.OK);
    }
}
